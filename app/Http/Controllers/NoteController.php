<?php

namespace App\Http\Controllers;

use App\Http\Requests\NoteRequest;
use App\Http\Requests\NoteUpdateRequest;
use App\Repositories\NoteRepository;

class NoteController extends Controller
{
    private $repository;

    public function __construct(NoteRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(){

        $data = $this->repository->index();

        return response()->json(['data'=>$data], 200);
    }

    public function save(NoteRequest $request){
        $this->repository->save($request);
        return response()->json(['message'=>'Note Saved'],200);
    }

    public function update(NoteUpdateRequest $request){
        $this->repository->update($request->validated());
        return response()->json(['message'=>'Note updated!'],200);

    }

    public function delete($noteId){
        $this->repository->delete($noteId);
    }
}
