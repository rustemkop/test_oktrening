<?php

namespace App\Interfaces;

interface notesInterface {
    public function index();
    public function save($request);
    public function update($request);
    public function delete($noteId);
}
