<?php


namespace App\Repositories;


use App\Interfaces\notesInterface;
use App\Models\Note;
use Mockery\Exception;

class NoteRepository implements notesInterface {
    public function index()
    {
        return Note::all();
    }

    public function save($request): Note
    {
        $note = Note::create(['text'=>$request->text]);
        return $note;

    }

    public function update($request)
    {
        try{
            $note = Note::find($request['id']);
            $note->update($request);

            return $note;
        }catch (\Throwable $e){
            return $e;
        }

    }

    public function delete($noteId)
    {
        try{
            $note =  Note::find($noteId);
            $note->delete();

            return $note;
        }catch (\Throwable $e){
            return $e;
        }

    }
}
