<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NoteController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [\App\Http\Controllers\AuthController::class, 'login'])->name('login');
Route::get('/logout', [\App\Http\Controllers\AuthController::class, 'logout']);
Route::post('/register', [\App\Http\Controllers\AuthController::class, 'register']);

Route::middleware('auth:api')->group(function (){

    Route::group(['prefix'=>'notes'], function (){
        Route::get('/', [NoteController::class, 'index']);
        Route::post('/', [NoteController::class, 'save']);
        Route::put('/', [NoteController::class, 'update']);
        Route::delete('/{id}', [NoteController::class, 'delete']);
    });
});
