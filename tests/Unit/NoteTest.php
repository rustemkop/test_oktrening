<?php

namespace Tests\Unit;

use App\Models\Note;
use App\Repositories\NoteRepository;
use Illuminate\Foundation\Testing\WithFaker;
use mysql_xdevapi\Collection;
use PHPUnit\Framework\TestCase;

class NoteTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function it_test_creating_note()
    {
        $data = collect();
        $data->text = 'salem salem salem';

        $noteRepo = new NoteRepository();
        $note = $noteRepo->save($data);
        $this->assertInstanceOf(Note::class, $note);
//        $this->assertEquals($data['text'], $note->text);
    }
}
